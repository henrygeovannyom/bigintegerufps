/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class BigInteger_UFPS {

    
    /**
     *  Numero="23456"
     * miNUmero={2,3,4,5,6};
     */
    private int miNumero[];
    
    public BigInteger_UFPS() {
    }

    public BigInteger_UFPS(int miNumero[]){
        this.miNumero = miNumero;
    }
    
    public BigInteger_UFPS(String miNumero) {
        int tamano = miNumero.length();
        this.miNumero = new int[tamano];
        for(int i = 0; i < tamano; i++){
            this.miNumero[i] = Integer.parseInt(String.valueOf(miNumero.charAt(i)));
        }
    }

    public int[] getMiNumero() {
        return miNumero;
    }
    
    
    /**
     * Mutiplica dos enteros BigInteger
     * @param dos
     * @return 
     */
    
    public BigInteger_UFPS multiply(BigInteger_UFPS dos)
    {
        int aux[], fina[];
        int tam = this.getMiNumero().length + dos.getMiNumero().length;
        aux = new int[tam - 1];
        fina = new int[tam];
        for(int i = this.getMiNumero().length - 1; i >= 0; i--){
            for(int j = dos.getMiNumero().length - 1; j >= 0; j--){
                aux[i+j] += this.getMiNumero()[i]*dos.getMiNumero()[j];
            }
        }
        int carry = 0;
        for(int k = tam - 1; k >= 1; k--){
            fina[k] = (aux[k-1] + carry)%10;
            carry = (aux[k-1] + carry)/10;
        }
        if(carry != 0){
            fina[0] = carry;
            BigInteger_UFPS big = new BigInteger_UFPS(fina);
            return big;
        }
        else{
            int other_fina[] = new int[tam - 1];
            for(int i = 0; i < tam-1; i++){
                other_fina[i] = fina[i+1];
            }
            BigInteger_UFPS big = new BigInteger_UFPS(other_fina);
            return big;
        }
    }    
         
    
    /**
     * Retorna la representación entera del BigInteger_UFPS
     * @return un entero
     */
    public int intValue()
    {
        int p = 0;
        int n = this.miNumero.length;
        for(int i = 0; i < n; i++){
            p += this.miNumero[i]*Math.pow(10, n - 1 - i);
        }
        return p;
    }

    @Override
    public String toString() {
        String impresion = "";
        for(int i = 0; i < this.miNumero.length; i++){
            impresion += (this.miNumero[i] + "");
        }
        return impresion;
    }
    
    
    
    
    
    
    
    
}
